#ifndef ROBOT_H
#define ROBOT_H

#include <ros/ros.h>
#include <std_srvs/Empty.h>
#include <geometry_msgs/Pose.h>
#include <laboratorio/robot_service.h>

class Robot
{
public:
  Robot();
  void moveRobot(laboratorio::robot_service);

private:
  ros::ServiceClient robot_client;
  ros::NodeHandle nh;
};

#endif // ROBOT_H
