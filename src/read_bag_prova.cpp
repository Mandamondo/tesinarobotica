#include <ros/ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Int32.h>
#include <std_srvs/Empty.h>
#include <std_msgs/Float64.h>
#include <sensor_msgs/Image.h>
#include <boost/array.hpp>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <boost/foreach.hpp>
#include <geometry_msgs/Twist.h>


using namespace std;

int main(int argc, char **argv)
{
  //-- Ros initialization
  ros::init(argc, argv, "main_node");
  ros::NodeHandle nh;

  ROS_INFO("Opened a rosbag.");
  double start =ros::Time::now().toSec();

  rosbag::Bag bag;
  bag.open("/home/iacopo/vel.bag", rosbag::bagmode::Read);

  std::vector<std::string> topics;
  topics.push_back(std::string("/turtle1/cmd_vel"));

  //bag.write("chatter", ros::Time::now(), str);
  //<geometry_msgs::Twist>("/cmd_vel", 1);

  rosbag::View view(bag, rosbag::TopicQuery(topics));

  BOOST_FOREACH(rosbag::MessageInstance const m, view)
  {
      geometry_msgs::Twist::ConstPtr s = m.instantiate<geometry_msgs::Twist>();
      if (s != NULL)
        std::cout << s->linear << std::endl;

      ros::Time test = m.getTime();
      std::cout << test.toSec() - start << std::endl;
  }

  bag.close();

  ROS_INFO("Closed a rosbag.");

  ros::spin();
  return 0;
}
