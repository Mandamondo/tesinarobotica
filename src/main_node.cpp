#include <ros/ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Int32.h>
#include <std_srvs/Empty.h>
#include <std_msgs/Float64.h>
#include <sensor_msgs/Image.h>
#include <boost/array.hpp>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <boost/foreach.hpp>
#include <geometry_msgs/Twist.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <typeinfo>
#include <cstring>
#include <opt_msgs/PoseRecognitionArray.h>
#include <opt_msgs/PoseRecognition.h>
#include <opt_msgs/PosePredictionResult.h>


using namespace std;

std::string bag_path, pose_path;
int track_number, verbose;

vector<string> split(const string& str, const string& delim)
{
    vector<string> tokens;
    size_t prev = 0, pos = 0;
    do
    {
        pos = str.find(delim, prev);
        if(pos == string::npos) pos = str.length();
        string token = str.substr(prev, pos-prev);
        if(!token.empty()) tokens.push_back(token);
        prev = pos + delim.length();
    }
    while (pos < str.length() && prev < str.length());
    return tokens;
}

void read_poses(std::vector<double>& t_start, std::vector<double>& t_end,
  std::vector<std::string>& raw_poses, std::istream& file)
{
  std::string line;
  while(std::getline(file, line))
  {
    //std::cout << "line:" << line << std::endl;
    vector<string> listToken = split(line, ";");

    for(int i = 0; i < listToken.size(); i++ )
      if(i == 0)
        t_start.push_back(atof(listToken[i].c_str()));
      else if(i == 1)
        t_end.push_back(atof(listToken[i].c_str()));
      else
        raw_poses.push_back(listToken[i]);
  }
}

int find_track_number(int track_number[], double track_time[], double time_inst)
{
  for (int i = 0; i < 7; i++)
    if (time_inst <= track_time[i])
      return track_number[i];
  return -1;
}

std::string find_pose_truth(const std::vector<double>& t_start, const std::vector<double>& t_end,
  const std::vector<std::string>& raw_poses, double message_time)
{
  for(int i = 0; i < raw_poses.size(); i++)
    if((message_time >= t_start[i]) && (message_time <= t_end[i]))
      return raw_poses[i];
  return "unknown";
}

int find_element(const std::vector<std::string>& array, std::string element)
{
  for(int i = 0; i < array.size(); i++)
    if(array[i] == element)
      return i;
  return -1;
}

void fill_confusion_matrix(vector<vector<double> >& confusion_matrix,
  const std::vector<std::string>& distinct_poses, std::string true_pose, std::string message_pose)
{
  int j_index = find_element(distinct_poses, true_pose);
  int i_index = find_element(distinct_poses, message_pose);
  confusion_matrix[i_index][j_index]++;
}

void initialize_confusion_matrix(vector<vector<double> >& confusion_matrix,
  const std::vector<std::string>& distinct_poses)
{
  confusion_matrix.resize(distinct_poses.size());
  for(int i = 0; i < distinct_poses.size(); ++i)
    confusion_matrix[i].resize(distinct_poses.size());
}


double recall(const vector<vector<double> >& confusion_matrix, int index)
{
  double sum = 0;
  for(int i = 0; i < confusion_matrix.size(); i++)
    if(i != index)
      sum += confusion_matrix[i][index];
  return confusion_matrix[index][index] / (confusion_matrix[index][index] + sum);
}

double precision(const vector<vector<double> >& confusion_matrix, int index)
{
  double sum = 0;
  for(int j = 0; j < confusion_matrix.size(); j++)
    if(j != index)
      sum += confusion_matrix[index][j];
  return confusion_matrix[index][index] / (confusion_matrix[index][index] + sum);
}

double accuracy(const vector<vector<double> >& confusion_matrix, int index)
{
  double sum = 0;
  double den = 0;
  double tn;
  for(int i = 0; i < confusion_matrix.size(); i++)
    for(int j = 0; j < confusion_matrix.size(); j++)
      den += confusion_matrix[i][j];
  for(int k = 0; k < confusion_matrix.size(); k++)
    if(k != index)
      sum += confusion_matrix[k][index] + confusion_matrix[index][k];
  tn = den - (sum + confusion_matrix[index][index]);
  return (tn + confusion_matrix[index][index]) / den;
}

double average_recall(const vector<vector<double> >& confusion_matrix)
{
  double sum = 0;
  for(int i = 0; i < confusion_matrix.size() - 1; i++)
  {
    sum += recall(confusion_matrix, i);
    if(verbose >= 2)
      cout << "Recall " << i << "-th: " << recall(confusion_matrix, i) << endl;
  }
  return sum / (confusion_matrix.size() - 1);
}

double average_precision(const vector<vector<double> >& confusion_matrix)
{
  double sum = 0;
  for(int i = 0; i < confusion_matrix.size() - 1; i++)
  {
    sum += precision(confusion_matrix, i);
    if(verbose >= 2)
      cout << "Precision " << i << "-th: " << precision(confusion_matrix, i) << endl;
  }
  return sum / (confusion_matrix.size() - 1);
}

double average_accuracy(const vector<vector<double> >& confusion_matrix)
{
  double sum = 0;
  for(int i = 0; i < confusion_matrix.size() - 1; i++)
  {
    sum += accuracy(confusion_matrix, i);
    if(verbose >= 2)
      cout << "Accuracy " << i << "-th: " << accuracy(confusion_matrix, i) << endl;
  }
  return sum / (confusion_matrix.size() - 1);
}

void print_confusion_matrix(const vector<vector<double> >& confusion_matrix)
{
  cout << "\nConfusion matrix: " << endl;
  for(int i = 0; i < confusion_matrix.size(); ++i)
  {
    for(int j = 0; j < confusion_matrix.size(); ++j)
      cout << std::setw(6) << confusion_matrix[i][j] << " ";
    cout << endl;
  }
}

void print_stats(const vector<vector<double> >& confusion_matrix)
{
  cout << endl;
  cout << "Average Precision: " << average_precision(confusion_matrix) << endl;
  cout << endl;
  cout << "Average Accuracy: " << average_accuracy(confusion_matrix) << endl;
  cout << endl;
  cout << "Average Recall: " << average_recall(confusion_matrix) << endl;
}

int main(int argc, char **argv)
{
  //Ros initialization
  ros::init(argc, argv, "main_node");
  ros::NodeHandle nh;

  //Read the paths from the config file
  nh.getParam("/main_node/bag_path", bag_path);
  nh.getParam("/main_node/pose_path", pose_path);
  nh.getParam("/main_node/verbose", verbose);
  nh.getParam("/main_node/track_number", track_number);

  //Read the times and poses from the pose file
  //and initialize the confusion matrix
  std::ifstream file(pose_path.c_str());
  std::vector<double> t_start, t_end;
  std::vector<std::string> raw_poses;
  std::string arr[] = {"arms_mid", "right_arm_up", "arms_up", "left_arm_pointing", "unknown"};
  std::vector<std::string> distinct_poses(arr, arr + sizeof(arr)/sizeof(std::string));
  int capture1_tracks_number [7] = { 2, 5, 4, 1, 4, 3, 2 };
  double capture1_tracks_time [7] = { 36, 38.16, 38.6, 39.67, 39.95, 47.5, 60 };
  int capture2_tracks_number [2] = { 1, 0 };
  double capture2_tracks_time [2] = { 31.45, 60 };
  vector<vector<double> > confusion_matrix;
  read_poses(t_start, t_end, raw_poses, file);
  initialize_confusion_matrix(confusion_matrix, distinct_poses);
  file.close();

  //Display the distinct poses
  if(verbose >= 4)
    for (int i = 0; i < distinct_poses.size() - 1; i++)
      std::cout << "distinct_poses [" << i << "]: " << distinct_poses[i] << std::endl;

  //Read the messages from the bag
  rosbag::Bag bag;
  bag.open(bag_path, rosbag::bagmode::Read);
  std::vector<std::string> topics;
  topics.push_back(std::string("/recognizer/poses"));
  rosbag::View view(bag, rosbag::TopicQuery(topics));

  double start;
  int i = 0;
  BOOST_FOREACH(rosbag::MessageInstance const m, view)
  {
      //Time of each msg
      ros::Time test = m.getTime();
      if(i == 0)
        start = test.toSec();
      double message_time = test.toSec() - start;

      //Read the message
      opt_msgs::PoseRecognitionArray::ConstPtr s = m.instantiate<opt_msgs::PoseRecognitionArray>();
      std::vector<opt_msgs::PoseRecognition> t = s->poses;
      opt_msgs::PoseRecognition t0;
      if(track_number == 1)
        t0 = t[find_track_number(capture1_tracks_number, capture1_tracks_time, message_time)];
      else
        t0 = t[find_track_number(capture2_tracks_number, capture2_tracks_time, message_time)];
      opt_msgs::PosePredictionResult u = t0.best_prediction_result;
      std::string message_pose = u.pose_name;

      std::string true_pose = find_pose_truth(t_start, t_end, raw_poses, message_time);

      if((i % 10 == 0) && (message_time > t_start[0]) && (verbose >= 5))
      {
         std::cout << "Message_pose: " << message_pose << " - True pose: " << true_pose << std::endl;
      }
      //ros::Duration(0.1).sleep();

      fill_confusion_matrix(confusion_matrix, distinct_poses, true_pose, message_pose);

      i++;
  }

  if(verbose >= 1)
    print_confusion_matrix(confusion_matrix);

  print_stats(confusion_matrix);

  bag.close();

  ros::spin();
  return 0;
}
